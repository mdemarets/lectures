# Applications of quantum mechanics

!!! summary "Learning goals"

    After following this course you will be able to:

    - explain the four topics of this course.
    - apply the theory of quantum mechanics to research problems.
    - perform calculations pertaining to applications.
    - read scientific literature such that results can be reproduced.

This is the beginning of an interacive version of the lecture notes and as such very much
work in progress.
Eventually, our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/applications-of-quantum-mechanics/lectures), especially do [let us know](https://gitlab.kwant-project.org/applications-of-quantum-mechanics/lectures/issues/new?issuable_template=typo) if you see a typo!
