from IPython import get_ipython
from IPython.display import set_matplotlib_formats

import matplotlib
import numpy as np


def configure_plotting():
    # Configure matplotlib
    ip = get_ipython()
    ip.config['InlineBackend']['figure_format'] = 'svg'
    # set_matplotlib_formats('svg')
    
    # Fix for https://stackoverflow.com/a/36021869/2217463
    ip.config['InlineBackend']['rc'] = {}
    ip.enable_matplotlib(gui='inline')

    matplotlib.rcParams['text.usetex'] = False
    matplotlib.rcParams['figure.figsize'] = (10, 7)
    matplotlib.rcParams['font.size'] = 16


def make_plotly_slider_fig(fig, params, param_name, slider_plots, index_active):
    """Create a plotly figure with a slider to select the different plotly traces
    in `slider_plots`.
    
    Parameters
    ----------
    fig: plotly Figure object
        The plotly figure object may already have traces plotted. These will not be
        affected by the newly generated slider
    params: array
        Parameter values corresponding to the different plots in `slider_plots`. Used
        to label the slider.
    param_name: str
        Parameter name used for labeling the slider.
    slider_plots: array
        Each entry in `slider_plot` can either be a single trace or an array of traces.
        The slider will switch between the different entries. Note that we must have
        `len(slider_plots) == len(params)`.
    index_active: int
        The entry in `slider_plots` that should be shown initially.
    """
    
    index = [ len(fig.data) ]
    for i, plot in enumerate(slider_plots):
        # check if the plot is an array of several plots
        try:
            for trace in plot:
                fig.add_trace(trace)
                fig.data[-1].visible = False if i != index_active else True
            index.append(index[-1] + len(plot))
            
        except TypeError:
            # not an iterable, so single trace
            fig.add_trace(plot)
            fig.data[-1].visible = False if i != index_active else True
            index.append(index[-1] + 1)
    
    # Create and add slider
    steps = []
    for i, (start, stop) in enumerate(zip(index[:-1], index[1:])):
        step = dict(
            method="restyle",
            args=["visible", np.array([False] * index[-1])],
            label=params[i]
        )
        step["args"][1][:index[0]] = True
        step["args"][1][start:stop] = True  # Toggle i'th trace to "visible"
        steps.append(step)
        
    sliders = [dict(
        active=index_active,
        currentvalue={"prefix": param_name + ": "},
        pad={"t": 50},
        steps=steps
        )]
    
    fig.update_layout(sliders=sliders)


from tempfile import TemporaryDirectory
from pathlib import Path

def to_svg_jshtml(anim, fps=None, embed_frames=True, default_mode=None):
    """Generate a JavaScript HTML representation of the animation but using the svg
       file format
    """
        
    if fps is None and hasattr(anim, '_interval'):
        # Convert interval in ms to frames per second
        fps = 1000 / anim._interval

    # If we're not given a default mode, choose one base on the value of
    # the repeat attribute
    if default_mode is None:
        default_mode = 'loop' if anim.repeat else 'once'

    # Can't open a NamedTemporaryFile twice on Windows, so use a
    # TemporaryDirectory instead.
    with TemporaryDirectory() as tmpdir:
        path = Path(tmpdir, "temp.html")
        writer = matplotlib.animation.HTMLWriter(fps=fps,
                                                 embed_frames=embed_frames,
                                                 default_mode=default_mode)
        writer.frame_format = 'svg'
        anim.save(str(path), writer=writer)
        jshtml = path.read_text()
            
        # the mime type of svg is wrong in matplotlib
        jshtml = jshtml.replace("data:image/svg", "data:image/svg+xml")
        
    return jshtml

    
def draw_classic_axes(ax, x=0, y=0, xlabeloffset=.1, ylabeloffset=.07):
    ax.set_axis_off()
    x0, x1 = ax.get_xlim()
    y0, y1 = ax.get_ylim()
    ax.annotate(
        ax.get_xlabel(), xytext=(x1, y), xy=(x0, y),
        arrowprops=dict(arrowstyle="<-"), va='center'
    )
    ax.annotate(
        ax.get_ylabel(), xytext=(x, y1), xy=(x, y0),
        arrowprops=dict(arrowstyle="<-"), ha='center'
    )
    for pos, label in zip(ax.get_xticks(), ax.get_xticklabels()):
        ax.text(pos, y - xlabeloffset, label.get_text(),
                ha='center', va='bottom')
    for pos, label in zip(ax.get_yticks(), ax.get_yticklabels()):
        ax.text(x - ylabeloffset, pos, label.get_text(),
                ha='right', va='center')
