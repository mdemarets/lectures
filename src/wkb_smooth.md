---
title: The WKB approximation
---

# A particle moving in a potential

!!! success "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Describe the motion of a classical particle in a potenial.

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Describe the motion of a quantum mechanical wave packet in a smooth or abrupt potential.

## Classical intuition


The WKB approximation is used to describe a particle of mass $m$ in a one-dimensional (1D) potential with some initial velocity. To call some intuition, let us recall the classical case: consider a particle of energy $E$ moving in a potential $V(x)$. In this situation, wthe total energy of the particle is conserved, and can be at every point $x$ be separated into kinetic and potential energy as $E = E_\text{kin}(x) + V(x)$.

If $E > V(x)$, then the particle will always have an energy larger than the potential at any point $x$. Hence, there will always be some non-zero kinetic energy at every point $x$. As a consequence, the particle will continuously move in one direction (determined by the initial velocity). This happens irrespective of the potential shape: for example, it does not matter whether the potential is smooth or changes abruptly.

![Classical particle with energy smaller than potential energy](figures/classical_scat1.svg)

If $E < V(x)$, a classical particle will reach a maximum position $x_0$ such that $E = V(x_0)$ and the kinetic energy becomes zero. Then, the particle will be reflected. Again, this will happen for any potential shape.

![Classical particle with energy larger than potential energy](figures/classical_scat2.svg)

In summary, in classical mechanics the global motion of a particle in a 1D potential is only determined by the value of the total energy compared to the potential $V(x)$.

## Quantum case

Let us now consider the quantum case: instead of a point particle, we now consider its quantum analogue, a wave packet. We will now let this wave packet time-evolve in different potentials.

### Smooth potential

First we consider the case where $V(x)$ is a smooth potential describing a dip. One can observe that the intuition from classical mechanics holds because the particle moves through the potential with a sightly increase in velocity along the dip.

```python inline
import common
from IPython.display import HTML, display
common.configure_plotting()
```


```python inline
from wkb import make_wave_packet_animation
import math

def pot_gauss(x):
    return -0.02*math.exp(-(x-1500.0)**2/200**2)

anim = make_wave_packet_animation(L=3500, pot_func=pot_gauss, 
                                  zero_pos=750, width=100,
                                  energy=0.01)

display(HTML(common.to_svg_jshtml(anim)))
```

### Abrupt potential

One the other hand, if $V(x)$ describes an abrupt potential, one can observe in the following animation that the wavefunction reflects back whenever it crosses an abrupt step.


```python inline

from wkb import make_wave_packet_animation
import math

def pot_step(x):
    if 1500 < x < 2200:
        return -0.02
    else:
        return 0

anim = make_wave_packet_animation(L=3500, pot_func=pot_step, 
                                  zero_pos=750, width=100,
                                  energy=0.01)

display(HTML(common.to_svg_jshtml(anim)))
```

This is an effect that is not present in the classical case.

## Summary 

For a quantum particle with $E > V(x)$:

* When a particle moves along a smooth potential, it resembles the classical case.
* When a particle moves along an abrupt potential, there's always some backreflection.