import kwant
import numpy as np
import matplotlib.pyplot as plt
"""
Code used to generate the plot of the probability currents found in wkb_tunel.md
"""

# Create system
a = 1
L = 200
W = 40
t = 1
V = 1

lat = kwant.lattice.square(a=1, norbs=1)

syst = kwant.Builder()

syst[(lat(x, y) for x in range(L) for y in range(W))] = 4 * t
syst[(lat(x, y) for x in range(80, L-80) for y in range(W))] = 4 * t + V
syst[lat.neighbors()] = -t
lead = kwant.Builder(kwant.TranslationalSymmetry((-a, 0)))
lead[(lat(0, j) for j in range(W))] = 4 * t
lead[lat.neighbors()] = -t
syst.attach_lead(lead)
syst.attach_lead(lead.reversed())

# Define scattering states
E0 = V - 0.7
E1 = V - 0.15
scattering_states_0 = kwant.wave_function(syst.finalized(), energy=E0)
scattering_states_1 = kwant.wave_function(syst.finalized(), energy=E1)
density = kwant.operator.Current(syst.finalized())

# Plot probability currents and potential profile
fig, ax = plt.subplots(ncols=1, nrows=3, figsize=(10, 10))
x = np.arange(0, 200)
y = np.heaviside(x-80, 0)*np.heaviside(-x+120, 0)
ax[0].plot(x, y, color='black', label=r'V(x)')
ax[0].hlines(y=E0, xmin=0, xmax=200, color='darkblue', linestyle='dashed', label='$E_0$')
ax[0].hlines(y=E1, xmin=0, xmax=200, color='red', linestyle='dashed', label='$E_1$')
ax[0].legend(fontsize=15)
ax[0].set_xlabel(r'$x$', fontsize=15)
ax[0].set_title(r'Potential profile', fontsize=15)
ax[1].set_title(r'Transmitted wave at $E=E_1$', fontsize=15)
ax[2].set_title(r'Reflected wave at $E=E_0$', fontsize=15)
i = 0
for axx in ax:
    if i > 0:
        axx.set_xlabel(r'$x$', fontsize=15)
        axx.set_ylabel(r'$y$', fontsize=15)
    axx.set_xticks([]);
    axx.set_yticks([]);
    i += 1
kwant.plotter.current(syst.finalized(), density((scattering_states_1(0)[0])), ax=ax[1])
kwant.plotter.current(syst.finalized(), density((scattering_states_0(0)[0])), ax=ax[2])
plt.savefig(fname='wkb_current.png')

"""
Previous plot

```python inline
import matplotlib.pyplot as plt
import numpy as np

E = 6
amplitude_left = 4.5
amplitude_right = 0.5

x = np.linspace(0, 5, 200)
left_side = np.linspace(0, 1, 200)
middle_side = np.linspace(1, 4, 200)
right_side = np.linspace(4, 5, 200)

y = 7*np.heaviside(x-1, 0)*np.heaviside(-(x-4), 0)*(2-(x - 2.5)**2/8)

prop_wave = E + amplitude_left*np.cos(4*left_side*np.pi)
transmitted_wave = E + amplitude_right*np.cos(4*left_side*np.pi)

one_over_l = -(1/3)*np.log((E+amplitude_left)/(E+amplitude_right))
decaying_wave = (E+amplitude_left)*np.exp((middle_side-1)*one_over_l)

fig, ax = plt.subplots(figsize=(10, 5))
ax.plot(x, y, color='black', label=r'V(x)')
ax.plot(left_side, prop_wave, color='darkblue')
ax.plot(middle_side, decaying_wave, color='darkblue', label=r'$\psi(x)$')
ax.plot(right_side, transmitted_wave, color='darkblue')
ax.hlines(y=E, xmin=0, xmax=5, color='red', linestyle='dashed', label='E')
ax.set_xticks([]);
ax.set_yticks([]);
ax.set_xlabel(r'$x$', fontsize=15)
ax.legend(fontsize=15)
fig.show()
```

"""