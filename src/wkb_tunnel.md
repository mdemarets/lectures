---
title: The WKB wave function
---

# Transport

```python inline
import common
from IPython.display import HTML, display
common.configure_plotting()
```


!!! success "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Describe the behavior of tunneling through a rectangular barrier
    - Write down the WKB wave function
    - Apply the connection formulas to connect the WKB wave functions across a turning point
    - Rewrite integrals


!!! summary "Learning goals"

    After this lecture you will be able to:

    - Write down the transmission probability of a tunneling problem.
    - Describe the effect of the potential on the probability current for a travelling wave.



## Setting: transport problem

Having looked at bound states before, we will now focus on quite the opposite situation: the case where far away from from the potential the energy is *larger* than the potential, i.e. $E>V(x)$ for $|x|\rightarrow \infty$. In such a case is *open*, we will not have bound states but there will be eigenstates for a continuous range of energies. Far away from some central potential, the eigenstates can be written as a superposition of plane waves. One particular choice for the eigenstate is to choose to have an incoming wave only one side (this is always possible), as shown in the following sketch:

![tunneling](figures/tunneling.svg)

Assuming that the potential becomes constant for some point away from the central potential, the wave function can be written as 
$$
\psi(x) = \left\{\begin{array}{cc}
A e^{ik_L x} + B e^{-ik_L x} & x \rightarrow -\infty,\\
F e^{ik_R x}, & x \rightarrow \infty.
\end{array}\right.
$$
where $k_L = \sqrt{\frac{2m}{\hbar^2} (E-V(x\rightarrow -\infty))}$ and $k_R =\sqrt{\frac{2m}{\hbar^2} (E-V(x\rightarrow \infty))}$ are the wave vectors on the left and right side. These can be different if the potential left and right are not equal.

In such a setting, the interesting question is how much of the incoming wave is transmitted to the right, and how much reflected to the left. In fact, you solved exactly such a system already in your first quantum mechanics class when you computed the tunnel probability through a square barrier! With WKB, we can do approximate solutions now for any kind of potential.

The central quantity of interest is the tunneling probability $T$. As in the example of the square tunnel barrier, it is generally defined as
$$
T = \left| \frac{F}{A} \right|^2 \frac{v_R}{v_L},
$$
where $v_L$ and $v_R$ are the velocities to the left and the right of the central potential. The reson why the velocity is entering is that transmission probability is with respect to the probability *current* - the question is how much of the incident current goes through or is reflected.

It is possible to get rid of the awkward velocity term by a small redefinition: We can instead write
$$
\psi(x) = \left\{\begin{array}{cc}
\frac{A}{\sqrt{v_L}} e^{ik_L x} + \frac{B}{\sqrt{v_R}} e^{-ik_L x} & x \rightarrow -\infty,\\
\frac{F}{\sqrt{v_R}} e^{ik_R x}, & x \rightarrow \infty.
\end{array}\right.
$$
The point is that with this convention, the waves to the left and right have a constant current if e.g. the coefficients $A$ and $F$ are the same. In fact, we can immediately see that for the WKB wave functions we have already chosen such a normalization:
$$
\psi(x)_{E > V(x)} \sim  \frac{A}{\sqrt{p(x)}}e^{\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'} + \frac{B}{\sqrt{p(x)}}e^{-\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'}\,,
$$
where $\frac{1}{\sqrt{p(x)}} \sim \frac{1}{\sqrt{v}}$. The tunnel probability is thus simply given by
$$
T = \left| \frac{F}{A}\right|^2\,.
$$

## $E > V(x)$: Reflectionless transport 

Let us start with the simplest case, where $E > V(x)$ for all $x$. For that case we already previously wrote down the [WKB wave function](wkb_wf.md) for all $x$. In particular, if we only consider an incoming wave from the left, we obtain
$$
\psi_\text{WKB}(x) \sim \frac{1}{\sqrt{p(x)}}e^{\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'}\,.
$$
We see here that there is no reflected wave, so the transmission probablity is $T=1$. Indeed, we argued initially that for a smooth potential there is no backreflection, so no surprises here!

It can still be interesting to think about how the wavefunction interacts with the potential. When [deriving the WKB wave function intuitively](wkb_wf.md#intuitive-derivation), we used current conservation to derive the amplitude of the wave function. In particular,
$$
j \sim A(x)^2 \frac{p(x)}{m}\,,
$$
where $A(x)$ was the amplitude of the wave function. We thus see that for regions where the momentum $p(x)$ decreases, i.e. for larger $V(x)$, the amplitude needs to increase. Conversely, when $p(x)$ increases, the amplitude needs to decrease. This phenomenon can be observed in the following animation:

```python inline
from wkb import wkb_static_animation
import numpy as np

x = np.linspace(500, 2500, 1000)
anim = wkb_static_animation(x, E=1.5)
display(HTML(common.to_svg_jshtml(anim)))
```

## Intuitive derivation for the tunneling case

The more interesting case is when there is some central part where $V(x) > E$, as then we will have finite reflection and a transmission $T<1$.

In this section, we will first give a intuitive, but not rigorous derivation of the tunneling probablity in WKB approximation. To this end, let us first consider the familiar case of a rectangular barrier. We know that in this case the wave function under the barrier consists of both an exponentially decaying and an exponentially growing part: $\psi(x) = C e^{-\kappa x} + D e^{+\kappa x}$. In the following interactive picture, you can explore how the wave function changes as a function of barrier height:

```python inline
from wkb import tunnel_animation
import numpy as np

def pot_step(A=0.1):
    def V(x):
        if 40 < x < 60:
            return A
        else:
            return 0
    return V

mags = np.linspace(0.035, 0.1, 20)
anim = tunnel_animation(mags=mags, potential_f=pot_step)
display(HTML(common.to_svg_jshtml(anim)))
```
We observe that as the barrier gets higher, the wave function in the central part becomes more and more dominated by the exponentially decaying part, $\psi(x) \sim C e^{-\kappa x}$. Even more,
the wave function to the right is suppressed due to this dominance of the decaying part. To a good approximation the wave function to the right of the barrier is then suppressed by $e^{-\kappa L}$ where $L$ is the barrier length. As a consequence, we can estimate the tunnel probability to be
$$
T \sim \left(e^{-\kappa L}\right)^2 = e^{-2 \kappa L}\,.
$$

We can use the same argument now for a tunnel barrier that has some smooth variation instead of a constant height:


To solve for the wavefunction in the region $x_1=0 < x < x_2=L$, we can use the WKB approximation. To account for an arbitrary potential shape, recall that the WKB wavefunction is,
$$
\psi_{WKB}(x) \sim \frac{1}{\sqrt{|p(x)|}} \left( C e^{ -\frac{1}{\hbar} \int_{0}^x |p(x')| d x'} + D e^{ \frac{1}{\hbar} \int_{0}^x |p(x')| d x'} \right).
$$
In the limit of a high barrier and/or long barrier, we can again approximate the wave function under the barrier by only the decaying part:
$$
\psi_{WKB}(x) \sim \frac{C}{\sqrt{|p(x)|}} e^{ -\frac{1}{\hbar} \int_{0}^x |p(x')| d x'}\,.
$$
Hence, the WKB tunneling probability is
$$
T = \sim e^{-2\gamma},\qquad \gamma = \frac{1}{\hbar}\int_0^L |p(x')|dx'. \tag{1}\label{eq:wkb_tunnel}
$$
This formula is in fact one of the most famous results of the WKB formulation, and very useful to estimate tunneling probablities


## Derivation of the WKB tunneling probability for a smooth barrier

The previous derivation lacks mathematical rigor. Using the connection formulas, we *can* actually solve the full problem in WKB approximation - and as we will see below we will exactly recover the WKB tunneling probability \eqref{eq:wkb_tunnel} in the limit of a high and wide barrier.

To this end, we'll use the transfer matrix method. Let us recall that the WKB wavefunction for propagating waves is,
$$
\psi(x) = \frac{1}{\sqrt{p(x)}} \left[ A \exp\left( \frac{i}{\hbar} \int_{x}^{x_1} p(x') d x'\right) + B \exp\left( -\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x'\right)\right].
$$
In order to use the connection formulas, $\psi$ must be expressed in the sine and cosine basis with coefficients $\tilde A$ and $\tilde B$. The correspondence between the coefficients around the turning points is show in the plot below.

```python inline
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-1, 1, 100)
y1 = x
E = 0
fig, ax = plt.subplots(figsize=(4, 4))
ax.plot(x, y1, color='black', label=r'V(x)')
ax.hlines(y=E, xmin=-1, xmax=1, color='red', label='E')
ax.set_xticks([]);
ax.set_yticks([]);
ax.vlines(x=0, ymin=-2, ymax=0, linestyle='dashed', color='black')
ax.scatter(0, 0, color='black', zorder=100)
ax.set_ylim(-1, 1);

ax.legend(fontsize=15)
ax.set_xlabel(r'$x_1$', fontsize=15)
ax.text(-0.5, E+0.1, r'$\tilde A$  $\tilde B$', fontsize=18, color='darkblue')
ax.text(0.2, E-0.2, r'$C_0$  $D_0$', fontsize=18, color='darkblue')
fig.show()
```

To the left of the turning point $x_1$, the wavefunction is given as,
$$
\psi(x)_{x < x_1} =  \frac{1}{\sqrt{p(x)}} \left[ \tilde A \sin\left( \frac{i}{\hbar} \int_{x}^{x_1} p(x') d x' + \frac{\pi}{4}\right) + \tilde B \cos\left( -\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x' + \frac{\pi}{4}\right)\right].
$$
Similarly, to the right of $x_1$,
$$
\psi(x)_{x < x_1} =  \frac{1}{\sqrt{p(x)}} \left[ C_0 \exp\left( \frac{1}{\hbar} \int_{x_1}^{x} p(x') d x'\right) + D_0 \exp\left( -\frac{1}{\hbar} \int_{x_1}^{x} p(x') d x'\right)\right].
$$
From the connection formulas, we recall that the coefficients are connect as,
$$
\tilde A = 2 C,\qquad \tilde B = D.
$$
This transformation can be expressed as a transfer matrix, that is,
$$
\left( \begin{array}{c}
\tilde A \\
\tilde B
\end{array} \right) = T_2 \left( \begin{array}{c}
C \\
D
\end{array} \right), \quad \text{where }
T_2 = \left( \begin{array}{cc}
2 & 0 \\
0 & 1
\end{array} \right)
$$
Let us express $\psi_{x< x_1}(x)$ in the plane wave basis. That is,
$$
\begin{split}
\psi_{x< x_1}(x) &= \frac{\tilde A}{\sqrt{p(x)}} \frac{1}{2 i} \left( e^{ \frac{i}{\hbar} \int_{x}^{x_1} |p(x')| d x' + \pi/4} - e^{ -\frac{i}{\hbar} \int_{x}^{x_1} |p(x')| d x' - \pi/4} \right)\\
&+\frac{\tilde B}{\sqrt{p(x)}} \frac{1}{2} \left( e^{ \frac{i}{\hbar} \int_{x}^{x_1} |p(x')| d x' + \pi/4} + e^{ -\frac{i}{\hbar} \int_{x}^{x_1} |p(x')| d x' - \pi/4}\right)\\
&= \left( \frac{\tilde A e^{i \pi/4}}{2i} + \frac{\tilde B e^{i \pi/4}}{2} \right) \frac{1}{\sqrt{p(x)}} e^{ \frac{i}{\hbar} \int_{x}^{x_1} |p(x')| d x'}\\
&+ \left( \frac{\tilde B e^{-i \pi/4}}{2} - \frac{\tilde A e^{-i \pi/4}}{2i} \right) \frac{1}{\sqrt{p(x)}} e^{ -\frac{i}{\hbar} \int_{x}^{x_1} |p(x')| d x'}.
\end{split}
$$
The connection between the two basis can be expressed using a transfer matrix. That is,
$$
\left( \begin{array}{c}
A \\
B
\end{array} \right) = T_1 \left( \begin{array}{c}
\tilde A \\
\tilde B
\end{array}\right), \quad \text{where }
T_1 = \frac{1}{2i} \left( \begin{array}{cc}
i e^{i \pi/4} & e^{i \pi/4} \\
i e^{-i \pi/4} & e^{-i \pi/4}
\end{array} \right).
$$

The next step is to find the relation between the coefficients $C_0$ and $D_0$ with $C_1$ and $D_1$. The wavefunction around each turning point can be written as,
$$
\begin{split}
\psi(x)_{x > x_1} &=  \frac{1}{\sqrt{p(x)}} \left[ C_0 \exp\left( - \frac{1}{\hbar} \int_{x_1}^{x} p(x') d x'\right) + D_0 \exp\left( \frac{1}{\hbar} \int_{x_1}^{x} p(x') d x'\right)\right],\\
\psi(x)_{x < x_2} &=  \frac{1}{\sqrt{p(x)}} \left[ C_1 \exp\left( - \frac{1}{\hbar} \int_{x}^{x_2} p(x') d x'\right) + D_1 \exp\left( -\frac{1}{\hbar} \int_{x}^{x_2} p(x') d x'\right)\right].
\end{split}
$$
Let us observe that each of the terms in both wavefunctions describes either an exponential decay or exponential increase. Then we match the coefficients that describe the same behaviour. Such consideration relates the coefficients as $C_0 \leftrightarrow D_1, \quad C_1 \leftrightarrow D_0$. That is,
$$
\begin{split}
C_0 \exp\left( - \frac{1}{\hbar} \int_{x_1}^{x} p(x') d x'\right) &= D_1 \exp\left( -\frac{1}{\hbar} \int_{x}^{x_2} p(x') d x'\right)\\
&\longrightarrow C_0 = D_1 \exp\left( -\frac{1}{\hbar} \int_{x_1}^{x_2} p(x') d x'\right) = e^\gamma D_1.
\end{split}
$$
A similar relation can be derived for the second pair of coefficients. That is,
$$
D_0 = e^{-\gamma} C_1.
$$
Expressing this relation as a transfer matrix, we find,
$$
\left( \begin{array}{c}
C_0 \\
D_0
\end{array} \right) = T_3 \left( \begin{array}{c}
C_1 \\
D_1
\end{array}\right), \quad \text{where }
T_3 = \left( \begin{array}{cc}
0 & e^{\gamma} \\
e^{-\gamma} & 0
\end{array} \right).
$$

In the second turning point, $x_2$, the slope has an opposite sign. By repeting the same derivation as before, one finds that the transfer matrix correspond to the inverse of the previous case.

```python inline
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-1, 1, 100)
y1 = -x
E = 0
fig, ax = plt.subplots(figsize=(4, 4))
ax.plot(x, y1, color='black', label=r'V(x)')
ax.hlines(y=E, xmin=-1, xmax=1, color='red', label='E')
ax.set_xticks([]);
ax.set_yticks([]);
ax.vlines(x=0, ymin=-2, ymax=0, linestyle='dashed', color='black')
ax.scatter(0, 0, color='black', zorder=100)
ax.set_ylim(-1, 1);

ax.legend(fontsize=15)
ax.set_xlabel(r'$x_2$', fontsize=15)
ax.text(-0.8, E-0.2, r'$C_1$  $D_1$', fontsize=18, color='darkblue')
ax.text(0.2, E+0.1, r'$\tilde E$  $\tilde F$', fontsize=18, color='darkblue')
fig.show()
```
That is,

$$
\left( \begin{array}{c}
C_1 \\
D_1
\end{array} \right) = T_2^{-1} \left( \begin{array}{c}
\tilde E \\
\tilde F
\end{array}\right).
$$
The last,
$$
\left( \begin{array}{c}
\tilde E \\
\tilde F
\end{array} \right) = T_1^{-1} \left( \begin{array}{c}
E \\
F
\end{array}\right)
$$
Therefore, the coefficients at both sides of the potential can be related as,
$$
\left( \begin{array}{c}
A \\
B
\end{array} \right) = T_1 T_2 T_3 T_2^{-1} T_1^{-1} \left( \begin{array}{c}
E \\
F
\end{array}\right) = \left(
\begin{array}{cc}
 \frac{i e^{-\gamma }}{4}-i e^{\gamma } & \dots \\
 \dots & \dots\\
\end{array}
\right) .
$$
Here we have omitted the terms that do not correspond to the tunneling amplitude. We can thus read off the tunneling probability:
$$
\begin{split}
T &= \left| \frac{E}{A}\right|^2 = \frac{1}{|\frac{i e^{-\gamma }}{4}-i e^{\gamma }|}\\
 & = \frac{1}{e^\gamma - e^{-\gamma}/4}\\
 &= \frac{e^{-2\gamma}}{1+e^{-2\gamma}/4}
\end{split}
$$
In the limit of a high and/or wide tunnel barrier $\gamma$ will be large and hence $e^{-2\gamma} \ll 1$. The denominator of the tunneling probability can thus be approximated as $1 + e^{-2\gamma}/4 \approx 1$, and we recover the result for the tunneling probability found in the previous section. That is,
$$
T \approx e^{-2\gamma},\qquad \gamma = \frac{1}{\hbar}\int_{x_1}^{x_2} |p(x')|dx'.
$$

## Summary

The following two ideas are the take-home message from this section:

* The transmission probability in the long and high barrier limit is $T\approx e^{-2\gamma}$.
* The density of the wavefunction increases when it is closer to the potential due to current conservation.